HISTSIZE=500
SAVEHIST=500
HISTFILE=~/.zsh_history
autoload -U colors
colors
PS1="%n@%{${fg_bold[red]}%}%m%{${fg_no_bold[default]}%} %D{%L:%M} %~%#"
bindkey -v

if [[ -r ~/.aliasrc ]]; then
	source ~/.aliasrc
fi
